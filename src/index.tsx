import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import "./index.scss";
import Spinner from "./Spinner";

// if (module.hot) {
//   module.hot.accept();
// }

class App extends React.Component {
  state = {
    lat: null,
    errorMessage: ""
  };

  componentDidMount() {
    console.log("Component was rendered to the screen");
    window.navigator.geolocation.getCurrentPosition(
      (position) => this.setState({ lat: position.coords.latitude }),
      (err) => this.setState({ errorMessage: err.message })
    );
  }

  componentDidUpdate() {
    console.log("Component was just updated");
  }

  render() {
    if (this.state.errorMessage && !this.state.lat) {
      return (
        <div className="centerText">
          <h1>Error: {this.state.errorMessage}</h1>
        </div>
      );
    }

    if (!this.state.errorMessage && this.state.lat) {
      return <SeasonDisplay lat={this.state.lat} />;
    }

    return (
      <Spinner message="Please forward the breakpoint ..." />
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
