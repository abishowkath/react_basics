import React from 'react';

interface messageObj { 
    message: string;
}

const Spinner = (props: messageObj) => {
    return (
        <div className='ui active dimmer'>
            <div className="ui big text loader">{props.message}</div>
        </div>
    );
}

Spinner.defaultProps = {
    message: "Loading..."
}

export default Spinner;