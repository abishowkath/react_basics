import React from 'react';
import './SeasonDisplay.scss'

interface position { 
  lat: number
}

const seasonConfig = {
  summer: {
    displayText: "Land of mostly sun",
    iconName: "sun"
  },
  winter: {
    displayText: "Winter is coming",
    iconName: "snowflake"
  }
}

const getSeason = (lat: number, month: number) => {
  if (month > 2 && month < 9) {
    return lat > 0 ? "summer" : "winter";
  } else {
    return lat > 0 ? "winter" : "summer";
  }
}

interface IconObj { 
  iconName: string,
  iconPosition: string
}

const Icon = ({iconName, iconPosition}: IconObj) => {
  return <i className={`${iconPosition} loading massive ${iconName} icon`}></i>
}

const SeasonDisplay = ({lat}: position) => {
  const season = getSeason(lat, new Date().getMonth());
  const { displayText, iconName } = seasonConfig[season];

  return (
    <div className={`season-display ${season}`}>
      <Icon iconName={iconName} iconPosition="icon-left" />
      <h1>{displayText}</h1>
      <Icon iconName={iconName} iconPosition="icon-right" />
    </div>
  );
};

export default SeasonDisplay;
